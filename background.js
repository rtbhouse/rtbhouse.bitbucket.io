fetch("https://rtbhouse.bitbucket.io/dictionary.json")
.then(response => response.json())
.then(data => {
    chrome.storage.local.set({dictionary: data});
})
.catch(error => {
    var url = chrome.runtime.getURL('./dictionary.json');
    fetch(url)
    .then((response) => response.json())
    .then((json) => {
        chrome.storage.local.set({dictionary: json});
    })
    .catch(error => console.log(error));
});
