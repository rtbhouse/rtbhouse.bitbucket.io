chrome.storage.local.get(['dictionary'], function(dictionary) {
	setTimeout(() => {
		var searchTerm = window.location.hostname;
		var results = dictionary.dictionary.filter(function (record) {
			return searchTerm == record.domain || searchTerm == "www." + record.domain
		});

		if (typeof results[0] === "undefined") {
			return;
		}
		var selectedTags = document.getElementsByTagName(results[0].tag);
		var search = {
			"text": results[0].innerHTML,
			"value": results[0].value,
			"id": results[0].id
		}
		var counter = 0;
		var interval = setInterval(function () {
			counter++;
			for (var i = 0; i < selectedTags.length; i++) {
				if (counter > 40) {
					clearInterval(interval);
					break;
				}
				if (((selectedTags[i].innerHTML).trim() == search.text && (typeof search.text !== "undefined")) ||
				   ((selectedTags[i].value) == search.value && (typeof search.value !== "undefined")) ||
				   ((selectedTags[i].id) == search.id && (typeof search.id !== "undefined"))) {
					console.log("click");
					console.log(selectedTags[i]);
					selectedTags[i].click();
					clearInterval(interval);
					break;
				}
			}
		}, 500);

	}, 5000);
});